# Mini Blav

To start your the app:

  * Install dependencies with `npm install`
  * Start express server with `npm start`

Now you can visit [`localhost:3000`](http://localhost:3000) from your browser.

Notes about production in `NOTES.txt`
