const path = require('path');
const webpack = require('webpack');

const endpoints = ['/api/*'];

function createApiProxy() {
  return endpoints
    .filter((endpoint) => {
      return endpoint !== '/api/v0/*';
    })
    .reduce((config, endpoint) => {
      config[endpoint] = {
        target: 'http://localhost:3000',
        changeOrigin: false
      }
      return config;
    }, {});
}

module.exports = {
  entry: ['whatwg-fetch','./src/index.jsx',],
  output: {
    path: path.resolve(__dirname, 'public'),
    filename: 'assets/bundle.js',
    publicPath:'/'
  },
  plugins: [
    new webpack.ProvidePlugin({})
  ],
  module: {
    loaders: [
      {
        test: /.jsx?$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        query: {
          presets: ['es2015', 'react']
        }
      },
      {
          test: /\.css$/,
          loader: 'style-loader?insertAt=top!css-loader'
      },
      {
          test: /\.(scss|sass)$/,
          loaders: ['style?insertAt=top', 'css', 'sass']
      },
      {
        test: /\.(eot.*|woff.*|ttf.*|svg.*|png|jpg|gif)$/,
        loader: 'file-loader',
        query: {
          name:'assets/[name].[ext]'
        }
      },
      {
        test: /\.html$/,
        // TODO: make the module configurable
        loader: 'html'
      },
      {
        test: /\.json$/,
        loader: "json-loader"
      }
    ]
  },
  debug: true,
  devServer: {
    contentBase: 'public',
    quiet: false,
    port: 3001,
    stats: { colors: true },
    proxy: createApiProxy()
  },
  resolve: {
    fallback: [path.join(__dirname, 'node_modules')]
  },
  resolveLoader: {
    root: path.join(__dirname, 'node_modules')
  }
}
