const express = require('express');
const router = express.Router();
const fibonacci = require('../src/fibonacci');

const Article = require('../models/article');

router.get('/', (req, res, next) => {
  Article.get((err, article) => {
    if (err) { return next(err); }
    fibonacci(34);
    res.json(article);
  });
});

router.post('/', (req, res, next) => {
  const data = req.body.article;
  const article = new Article(data.title, data.content);
  Article.save(article, (err) => {
    if (err) { return next(err); }
    res.json({message: 'Article saved'});
  })
})

module.exports = router;
