import React from 'react';

const ArticleViewer = (props) => {

  return (
    <div className="row">
      <h1>{props.article.title}</h1>
      <div dangerouslySetInnerHTML={{__html: props.article.content}} />
      <button
        onClick={() => props.toggleEdit()}
        className="btn btn-default">Edit Article</button>
    </div>
  );
};

export default ArticleViewer;
