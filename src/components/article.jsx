import React, { Component } from 'react';
import ArticleViewer from './article_viewer.jsx';
import ArticleEditor from './article_editor.jsx';

class Article extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isEditing: false
    };
  }

  cancelEdit() {
    this.setState({isEditing: false});
  }

  toggleEdit() {
    this.setState({isEditing: !this.state.isEditing});
  }

  render() {
    const save = (article)=> {
      this.props.saveArticle(article);
    }
    return !this.state.isEditing ?
      <ArticleViewer
        article={this.props.article}
        toggleEdit={() => this.toggleEdit()} /> :
      <ArticleEditor
        article={this.props.article}
        saveArticle={save}
        cancelEdit={() => this.cancelEdit()}/>;
  }
}

export default Article;
