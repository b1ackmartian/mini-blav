import React from 'react';
import TextEditor from './text_editor.jsx';

const ArticleEditor = (props) => {
  const save = (newContent) => {
    props.saveArticle({
      title: props.article.title,
      content: newContent
    });
  }

  return (
    <div className="row">
      <h1>{props.article.title} (editing)</h1>
      <TextEditor
        content={props.article.content}
        save={save}/>
      <button
        onClick={() => props.cancelEdit()}
        className="btn btn-default">Stop Editing Article</button>
    </div>
  );
};

export default ArticleEditor;
