import React, { Component } from 'react';
import {convertFromHTML, Editor, EditorState, ContentState} from 'draft-js';
import { stateToHTML } from 'draft-js-export-html';

class TextEditor extends Component {
  constructor(props) {
    super(props);
    const blocksFromHTML = convertFromHTML(props.content);
    const state = ContentState.createFromBlockArray(blocksFromHTML);
    this.state = {editorState: EditorState.createWithContent(state)};
    this.onChange = (editorState) => {
      this.setState({editorState});
      const html = stateToHTML(editorState.getCurrentContent());
      props.save(html);
    }
  }

  render() {
    return (
        <Editor
          editorState={this.state.editorState}
          onChange={this.onChange}
          ref="text-editor"/>
    );
  }
};

export default TextEditor;
