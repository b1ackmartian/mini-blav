import Bootstrap from 'bootstrap/dist/css/bootstrap.css';
import styles from './style.scss';

import _ from 'lodash';
import 'whatwg-fetch';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Article from './components/article.jsx';
const articlesUrl = '/api/articles';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      article: {}
    }
    this.getArticle();
  }

  getArticle() {
    const options = {
      method: 'GET'
    };
    fetch(articlesUrl, options)
      .then((response) => {
        return response.json();
      })
      .then((article) => {
        this.setState({article})
      })
      .catch((err) => {
        console.error(err);
      });
  }

  saveArticle(articleJson) {
    console.log(articleJson);
    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        article: articleJson
      })
    }
    fetch(articlesUrl, options)
      .then((response) => {
        return response.json();
      })
      .then((json) => {
        this.getArticle();
      })
      .catch((err) => {
        console.error(err);
      });
  }

  render() {
    return (
      <div className="container">
        <Article
          article={this.state.article}
          saveArticle={(article) => this.saveArticle(article)}/>
      </div>
    );
  }
}

// ReactDOM.render(<App />, document.getElementById('main'));
ReactDOM.render(<App />, document.querySelector('#main'));
