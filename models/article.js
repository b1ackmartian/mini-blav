const fs = require('fs');
const path = require('path');
const cheerio = require('cheerio');

class Article {
  constructor(title, content) {
    this.title = title;
    this.content = content;
  }

  static get(cb) {
    fs.readFile(path.join(__dirname, '../data/latest_plane_crash'), (err, data) => {
      if (err) { return cb(err); }
      const $ = cheerio.load(data);
      const title = $('body h1.title').text();
      const content = $('body div.content').html();
      cb(null, new Article(title, content));
    });
  }

  static save(article, cb) {
    const template = `
      <html>
        <body>
          <h1 class="title"></h1>
          <div class="content"></div>
        </body>
      </html>
    `;
    const $ = cheerio.load(template);
    $('body h1.title').text(article.title);
    $('body div.content').html(article.content);

    fs.writeFile(path.join(__dirname, '../data/latest_plane_crash'), $.html(), (err) => {
      if (err) { return cb(err); }
      cb(null);
    });
  }
}

module.exports = Article;
